<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        // Only logged in users can access the dashboard
        $this->middleware(['auth']);
    }

    public function index()
    {
        return view('dashboard');
    }
}
