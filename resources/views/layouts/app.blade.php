<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Posts</title>

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body class="bg-gray-200">
        <nav class="p-6 bg-yellow-300 flex justify-between mb-6">
            <ul class="flex items-center">
                <li class="hover:bg-yellow-400 rounded">
                    <a href="{{ route('home') }}" class="p-3">Home</a>
                </li>
                <li class="hover:bg-yellow-400 rounded">
                    <a href="{{ route('dashboard') }}" class="p-3">Dashboard</a>
                </li>
                <li class="hover:bg-yellow-400 rounded">
                    <a href="{{ route('posts') }}" class="p-3">Post</a>
                </li>
            </ul>

            <ul class="flex items-center">
                @auth
                    <li class="hover:bg-yellow-400 rounded">
                        <a href="" class="p-3">{{ auth()->user()->name }}</a>
                    </li>
                    <li class="hover:bg-yellow-400 rounded">
                        <form action="{{ route('logout') }}" method="post" class="p-3 inline">
                            @csrf
                            <button type="submit">Logout</button>
                        </form>
                    </li>
                @endauth

                @guest
                    <li class="hover:bg-yellow-400 rounded">
                        <a href="{{ route('login') }}" class="p-3">Login</a>
                    </li>
                    <li class="hover:bg-yellow-400 rounded">
                        <a href="{{ route('register') }}" class="p-3">Register</a>
                    </li>
                @endguest
            </ul>
        </nav>
        @yield('content')
    </body>
</html>
