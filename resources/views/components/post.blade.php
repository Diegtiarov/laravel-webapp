@props(['post' => $post])

<div class="bg-yellow-400 rounded-lg mb-4">
    <a href="{{ route('users.posts', $post->user) }}" class="font-bold hover:bg-yellow-300 rounded-lg ml-3 mr-3"> {{ $post->user->name }} </a> <span class="text-gray-600
    text-sm"> {{ $post->created_at->diffForHumans() }} </span>

    <p class="ml-2 mb-2"> {{ $post->body }} </p>

    @can('delete', $post)
        <div>
            <ul class="flex items-start gap-2 ml-1">
                @can('edit', $post)
                <li>
                    <form action="{{ route('posts.edit', $post) }}" method="post" class="hover:bg-yellow-300 rounded-lg">
                        @csrf
                        <button type="submit" class="text-blue-500">Edit</button>
                    </form>
                </li>
                @endcan

                <li>
                    <form action="{{ route('posts.destroy', $post) }}" method="post" class="hover:bg-yellow-300 rounded-lg">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="text-blue-500">Delete</button>
                    </form>
                </li>
            </ul>
        </div>
    @endcan

    <div class="flex items-center ml-1">
        @auth
            @if (!$post->likedBy(auth()->user()))
                <form action="{{ route('posts.likes', $post) }}" method="post" class="hover:bg-yellow-300 rounded-lg mr-1">
                    @csrf
                    <button type="submit" class="text-blue-500">Like</button>
                </form>
            @else
                <form action="{{ route('posts.likes', $post) }}" method="post" class="hover:bg-yellow-300 rounded-lg mr-1">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="text-blue-500">Unlike</button>
                </form>
            @endif
        @endauth

        <span> {{ $post->likes->count() }} {{ Str::plural('like', $post->likes->count()) }}</span>
    </div>
</div>
