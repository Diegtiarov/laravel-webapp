@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-8/12 bg-yellow-300 p-6 rounded-lg">
            <form action="{{ route('posts.update', $post->id) }}" method="post" class="mb-4">
                @csrf
                @method('PUT')
                @auth
                    <div class="mb-4">
                        <label for="body" class="sr-only">Body</label>

                        <textarea name="body" id="body" cols="40" rows="3" class="bg-gray-100
                        border-2 w-full p-4 rounded-lg @error('body') border-red-500 @enderror">{{ $post->body }}</textarea>

                        @error('body')
                            <div class="text-red-500 mt-2 text-sm">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div>
                        <button type="submit" class="bg-blue-500 hover:bg-blue-400
                        text-white px-4 py-2 rounded font-medium">Edit</button>
                    </div>
                @endauth
            </form>
        </div>
    </div>
@endsection
